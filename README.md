## SunCorpAssignment

This is simple Kotlin MVP demo. Here we are using following technologies

1. Kotlin
2. Clean MVP by Google
3. Retrofit by Square
4. Dagger 2 by Google
5. Room persistence
6. Spek for Unit testing.
7. Kotlin Extension

---

#Code Highlights

##Transaction (Data class)

    @Entity(tableName = "transaction_data")
    data class Transaction(
            @PrimaryKey
            val id: Int,
            val description: String,
            val amount: Double,
            @ColumnInfo(name = "date")
            val effectiveDate: String
    ) 

Transaction named model to parse data fetched from api service and defined column names to be used in database table with some properties.

##TransactionDao (Data Access Object)



    @Dao
    interface TransactionDao {
       @Query("SELECT * from transaction_data")
       fun getTransactions(): List<Transaction>

       @Insert(onConflict = REPLACE)
       fun insertTransaction(transactions: List<Transaction>)

       @Query("DELETE from transaction_data")
       fun deleteAllTransactions();
    }


This file is defined as DAO to interact with database table to fetch and insert entries


## BaseActivity

    abstract class BaseActivity<P : BasePresenter<IBaseView>> : IBaseView, AppCompatActivity() {
    
        lateinit var presenter: P
        
    
        override fun getContext(): Context {
            return this
        }
    }

BaseActivity is the base class for activities to extend as it extends BasePresenter, IBaseView along with AppCompatActivity already.


## BasePresenter

    abstract class BasePresenter<out V : IBaseView>(val view: V) {
    
    
        abstract fun onViewCreated()
        
        abstract fun onViewDestroyed()
    
    }
    
  Build Info
  
  Android Studio - 3.1.3, Compile SDK - 27, MinSDK - 15, Target - 27
  Libraries Used
  
  Android Support Libraries, Dagger 2, Retrofit, RecyclerView, CardView, Room, LiveData, RxJava, RxAndroid,Spek.