package com.sun.corp

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.whenever
import corp.sun.com.suncorpkotlintest.database.TransactionDao
import corp.sun.com.suncorpkotlintest.model.Transaction
import corp.sun.com.suncorpkotlintest.network.ApiService
import corp.sun.com.suncorpkotlintest.presenters.TransactionPresenter
import corp.sun.com.suncorpkotlintest.views.ITransactionView
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object TransactionPresenterTest : Spek({
    given("a TransactionPresenter") {
        val apiService: ApiService = mock()
        val transacationView: ITransactionView = mock()
        val transactionDao: TransactionDao = mock()
        val context: Context = mock()


        beforeEachTest {
            whenever(transacationView.getContext()).thenReturn(context)
        }


        afterEachTest {
            reset(apiService)
            reset(transactionDao)
        }

        on("fetch transaction from database") {


            it("transactions is empty") {
                val transactionPresenter = TransactionPresenter(transacationView)
                whenever(transactionDao.getTransactions()).thenReturn(listOf<Transaction>())
                transactionPresenter.fetchTransactionsFromDB()
                transactionPresenter.fetchTransactionsFromAPI()

            }

            it("transactions avaiable") {
                val transactionPresenter = TransactionPresenter(transacationView)
                val TRANSACTIONS = listOf(Transaction(1, "desc 1", 55.5, "20-11-2017"),
                        Transaction(2, "desc 2", 55.8, "22-11-2017"),
                        Transaction(3, "desc 3", 55.9, "23-11-2017"))
                whenever(transactionDao.getTransactions()).thenReturn(TRANSACTIONS)
                transactionPresenter.fetchTransactionsFromDB()

            }

        }

        on("fetch transaction from network") {
            val TRANSACTIONS = listOf(Transaction(1, "desc 1", 55.5, "20-11-2017"),
                    Transaction(2, "desc 2", 55.8, "22-11-2017"),
                    Transaction(3, "desc 3", 55.9, "23-11-2017"))

            it("when Internet is available") {
                val transactionPresenter = TransactionPresenter(transacationView)
                transactionPresenter.fetchTransactionsFromAPI()
            }

        }

        on("fetch transaction from network and save in Database") {
            val transactionPresenter = TransactionPresenter(transacationView)
            it("when Internet is available") {
                transactionPresenter.fetchTransactionsfromNetworkandSaveInDb()
            }
        }
    }
})