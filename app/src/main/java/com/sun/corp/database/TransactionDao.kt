package corp.sun.com.suncorpkotlintest.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import corp.sun.com.suncorpkotlintest.model.Transaction

@Dao
interface TransactionDao {

    @Query("SELECT * from transaction_data")
    fun getTransactions(): List<Transaction>

    @Insert(onConflict = REPLACE)
    fun insertTransaction(transactions: List<Transaction>)

    @Query("DELETE from transaction_data")
    fun deleteAllTransactions();
}