package corp.sun.com.suncorpkotlintest.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import corp.sun.com.suncorpkotlintest.model.Transaction

@Database(entities = arrayOf(Transaction::class), version = 1, exportSchema = false)
abstract class TransactionDatabase : RoomDatabase() {

    abstract fun transactionDao(): TransactionDao
}

