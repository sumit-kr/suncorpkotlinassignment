package corp.sun.com.suncorpkotlintest.presenters

import com.sun.corp.R
import corp.sun.com.suncorpkotlintest.dagger.components.DaggerPresenterComponent
import corp.sun.com.suncorpkotlintest.database.TransactionDao
import corp.sun.com.suncorpkotlintest.model.Transaction
import corp.sun.com.suncorpkotlintest.network.ApiService
import corp.sun.com.suncorpkotlintest.views.ITransactionView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Presenter that present the Transaction view.
 * @param view the Transaction view to be presented by the presenter
 * @property apiService the API interface implementation
 * @property disposable the subscription to the API call
 * @property transactionDao the dao of Database
 */
open class TransactionPresenter(view: ITransactionView) : BasePresenter<ITransactionView>(view) {

    private var disposable: Disposable? = null;

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var transactionDao: TransactionDao

    init {
        injectDependency()
    }

    /**
     * Injects the required dependencies
     */
    fun injectDependency() {
        val component = DaggerPresenterComponent.builder()
                .baseView(view)
                .build()
        component.inject(this)
    }

    override fun onViewCreated() {
        fetchTransactionsFromDB()
    }

    /*
     * Fetch transactions from database
     * if transactions available in database display on View
     * else fetch transactions from network
     */
    fun fetchTransactionsFromDB() {

        disposable = Observable.fromCallable({ transactionDao.getTransactions() })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { transactionList -> if (transactionList.isEmpty()) fetchTransactionsFromAPI() else view.showTransactions(transactionList) },
                        { _ -> view.showError(view.getContext().resources.getString(R.string.error_msg)) }
                )
    }


    /*
     *First check Internet Connection if connection available
     * Fetch transactions from api and insert into database
     */
    fun fetchTransactionsFromAPI() {
        view.showProgressDialog()
        disposable = fetchTransactionsfromNetworkandSaveInDb().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { view.hideProgressDialog() }
                .subscribe(
                        { transactionList -> view.showTransactions(transactionList) },
                        { _ -> view.showError(view.getContext().resources.getString(R.string.error_msg)) }
                )

    }

    /*
    * Fetch transactions from Network and insert into database
    */
    fun fetchTransactionsfromNetworkandSaveInDb(): Observable<List<Transaction>> {
        return apiService.getTransactions()
                .flatMap { transactionList -> Observable.fromCallable({ transactionDao.insertTransaction(transactionList);transactionList }) }
    }


    override fun onViewDestroyed() {
        disposable!!.dispose()
    }

}

