package corp.sun.com.suncorpkotlintest.presenters

import corp.sun.com.suncorpkotlintest.views.IBaseView

/**
 * Base presenter for all presenter, should extends BasePreseneter .
 * @param V the type of the View the presenter is based on
 * @property view the view the presenter is based on
 */
abstract class BasePresenter<out V : IBaseView>(val view: V) {

    /**
     * This method may be called when the presenter view is created
     */
    abstract fun onViewCreated()

    /**
     * This method may be called when the presenter view is destroyed
     */
    abstract fun onViewDestroyed()

}

