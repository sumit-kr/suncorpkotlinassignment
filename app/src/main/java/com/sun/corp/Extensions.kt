package corp.sun.com.suncorpkotlintest

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.widget.Toast
import com.sun.corp.activities.TransactionDialogFragment
import java.text.SimpleDateFormat
import java.util.*

/*
 * Checks internet is avilable or not
 * @return Boolean
 */
fun Context.isConnectedToInternet(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo

    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

/*
 * Show toast message to notify
 * @param msg
 */
fun Context.showToast(msg: String) {
    return Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

/*
 * Format date into SimpleDateFormat
 * @return String
 */
fun String.formatDate(): String {
    val initialFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'", Locale.ENGLISH)
    val format = SimpleDateFormat("yyyy-M-dd", Locale.US)
    val date = initialFormat.parse(this)
    return format.format(date).toString()
}

/*
 * Show Dialog Fragment
 * @param bundle
 */
fun FragmentActivity.showDialogFragment(bundle: Bundle) {
    val transc = supportFragmentManager.beginTransaction()

    val prevFrag = supportFragmentManager.findFragmentByTag("dialog")
    if (prevFrag != null) {
        transc.remove(prevFrag)
    }

    val transactionDialogFragment = TransactionDialogFragment()
    transactionDialogFragment.arguments = bundle
    transactionDialogFragment.show(transc, "dialog")
}