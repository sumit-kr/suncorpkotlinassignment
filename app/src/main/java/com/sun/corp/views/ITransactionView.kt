package corp.sun.com.suncorpkotlintest.views

import corp.sun.com.suncorpkotlintest.model.Transaction


/**
 * Interface have method to show transactions and
 * show error message if any.
 */
interface ITransactionView : IBaseView {

    /**
     * Show all transactions in the view
     * @param transactions the list of transactions
     */
    fun showTransactions(transactions: List<Transaction>)

    /**
     * Displays an error in the view
     * @param msg the error to display in the view
     */
    fun showError(msg: String)
}