package corp.sun.com.suncorpkotlintest.views

import android.content.Context

/**
 * Base view any view must implement.
 */
interface IBaseView {

    fun getContext(): Context

    fun showProgressDialog();

    fun hideProgressDialog();

    fun showMessage(msg: String)
}