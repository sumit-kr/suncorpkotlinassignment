package corp.sun.com.suncorpkotlintest.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import java.text.SimpleDateFormat
/**
 * Data class  provides a model for transaction implements Parcelable
 * @constructor Sets all properties of the transaction
 * @property id the unique identifier of the transaction
 * @property description the description of the transaction
 * @property amount the transaction of the transaction
 * @property effectiveDate the transaction of the transaction
 */
@Entity(tableName = "transaction_data")
data class Transaction(
        @PrimaryKey
        val id: Int,
        val description: String,
        val amount: Double,
        @ColumnInfo(name = "date")
        val effectiveDate: String
) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(description)
        parcel.writeDouble(amount)
        parcel.writeString(effectiveDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Transaction> {
        override fun createFromParcel(parcel: Parcel): Transaction {
            return Transaction(parcel)
        }

        override fun newArray(size: Int): Array<Transaction?> {
            return arrayOfNulls(size)
        }
    }

}