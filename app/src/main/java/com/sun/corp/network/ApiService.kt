package corp.sun.com.suncorpkotlintest.network

import corp.sun.com.suncorpkotlintest.model.Transaction
import corp.sun.com.suncorpkotlintest.utils.BASE_URL
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {

    /**
     * Get the list of the transactions from the API
     */
    @GET("/transactions")
    fun getTransactions(): Observable<List<Transaction>>

    companion object {

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }


}