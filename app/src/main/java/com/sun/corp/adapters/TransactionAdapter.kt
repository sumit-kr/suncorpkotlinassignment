package corp.sun.com.suncorpkotlintest.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sun.corp.R
import corp.sun.com.suncorpkotlintest.formatDate
import corp.sun.com.suncorpkotlintest.model.Transaction
import kotlinx.android.synthetic.main.item_transaction.view.*

/**
 * Adapter for the list of the transactions
 * @property context Context in which the application is running
 */
class TransactionAdapter(val context: Context, val transactions: List<Transaction>) :
        RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_transaction,
                parent, false))
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindData(transactions[position])
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    /**
     * ViewHolder of the adapter
     */
    class TransactionViewHolder(view: View) : ViewHolder(view) {

        fun bindData(transaction: Transaction) {

            with(itemView) {
                txt_desription.text = transaction.description
                txt_amount.text = transaction.amount.toString()
                txt_date.text = transaction.effectiveDate.formatDate()
            }
        }
    }
}