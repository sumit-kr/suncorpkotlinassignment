package corp.sun.com.suncorpkotlintest.dagger.components

import corp.sun.com.suncorpkotlintest.dagger.modules.ContextModule
import corp.sun.com.suncorpkotlintest.dagger.modules.PresenterModule
import corp.sun.com.suncorpkotlintest.presenters.TransactionPresenter
import corp.sun.com.suncorpkotlintest.views.IBaseView
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [ContextModule::class, PresenterModule::class])
interface PresenterComponent {

    /**
     * Injects required dependencies into the specified TransactionPresenter.
     * @param presenter TransactionPresenter in which to inject the dependencies
     */
    fun inject(presenter: TransactionPresenter)

    @Component.Builder
    interface Builder {

        fun build(): PresenterComponent

        @BindsInstance
        fun baseView(baseView: IBaseView): Builder

    }

}