package corp.sun.com.suncorpkotlintest.dagger.modules

import android.content.Context
import corp.sun.com.suncorpkotlintest.views.IBaseView
import dagger.Module
import dagger.Provides
/**
 * Module provide dependencies about Context
 */
@Module
class ContextModule {

    /**
     * Provides the Context
     * @param baseView the BaseView used to provides the context
     * @return the Context to be provided
     */
    @Provides
    fun provideContext(baseView: IBaseView): Context {
        return baseView.getContext()
    }

}