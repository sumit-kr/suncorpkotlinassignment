package corp.sun.com.suncorpkotlintest.dagger.modules

import android.arch.persistence.room.Room
import android.content.Context
import corp.sun.com.suncorpkotlintest.database.TransactionDao
import corp.sun.com.suncorpkotlintest.database.TransactionDatabase
import corp.sun.com.suncorpkotlintest.network.ApiService
import corp.sun.com.suncorpkotlintest.utils.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = arrayOf(ContextModule::class))
class PresenterModule {

    /**
     * Provides the network implementation.
     * @return the APIService implementation.
     */
    @Provides
    fun provideAPIService(): ApiService {
        return ApiService.create()
    }

    /**
     * Initialize Room database object
     * @param context context in which application runs
     */
    @Provides
    @Singleton
    fun provideDatabase(context: Context) = Room.databaseBuilder(context, TransactionDatabase::class.java, DATABASE_NAME).build()

    /**
     * Provides Dao to access database
     * @param transactionDatabase transaction database object used to Query on transaction
     * @return transactionDao
     */
    @Provides
    @Singleton
    fun provideTransactionDao(transactionDatabase: TransactionDatabase): TransactionDao {
        return transactionDatabase.transactionDao()

    }


}