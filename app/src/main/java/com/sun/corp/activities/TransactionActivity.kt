package corp.sun.com.suncorpkotlintest.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.sun.corp.R
import corp.sun.com.suncorpkotlintest.OnItemClickListener
import corp.sun.com.suncorpkotlintest.adapters.TransactionAdapter
import corp.sun.com.suncorpkotlintest.addOnItemClickListener
import corp.sun.com.suncorpkotlintest.model.Transaction
import corp.sun.com.suncorpkotlintest.presenters.TransactionPresenter
import corp.sun.com.suncorpkotlintest.showDialogFragment
import corp.sun.com.suncorpkotlintest.showToast
import corp.sun.com.suncorpkotlintest.utils.SimpleDividerItemDecoration
import corp.sun.com.suncorpkotlintest.views.ITransactionView
import kotlinx.android.synthetic.main.activity_main.*

/*
 * TransactionActivity extend BaseActivity and Implements ITransactionView
 * Mainly used to display lists of transactions.
 */
class TransactionActivity : BaseActivity<TransactionPresenter>(), ITransactionView {

    lateinit var transactions: List<Transaction>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = instantiatePresenter()

        presenter.onViewCreated()

        rv_transactions.layoutManager = LinearLayoutManager(this)
        rv_transactions.addItemDecoration(SimpleDividerItemDecoration(this))


        // On click event on recyclerview
        rv_transactions.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {

                // Get a transaction and bundle it to pass in dialog fragment
                val transaction = transactions[position]
                val bundle = Bundle()
                bundle.putParcelable("transaction", transaction)

                //Show dialog fragment
                showDialogFragment(bundle)
            }

        })
    }

    /*
    *   Show progress indicator
   */
    override fun showProgressDialog() {
        progress.visibility = View.VISIBLE
    }

    /*
    * Hide progress indicator
   */
    override fun hideProgressDialog() {
        if (progress.visibility == View.VISIBLE) {
            progress.visibility = View.GONE
        }
    }

    /*
    *   Display toast message
   */
    override fun showMessage(msg: String) {
        showToast(msg)
    }

    /*
    *  Initialize transaction adapter and inflate transactions items
   */
    override fun showTransactions(transactions: List<Transaction>) {
        this.transactions = transactions
        val adapter = TransactionAdapter(this, this.transactions)
        rv_transactions.adapter = adapter
    }


    /*
    *   Show error msg like Network error and Database error
   */
    override fun showError(msg: String) {
        hideProgressDialog()
        showToast(msg)
    }

    /*
    *   Initialize transaction presenter
   */
    fun instantiatePresenter(): TransactionPresenter {
        return TransactionPresenter(this)
    }
}