package corp.sun.com.suncorpkotlintest.activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import corp.sun.com.suncorpkotlintest.presenters.BasePresenter
import corp.sun.com.suncorpkotlintest.views.IBaseView

/**
 *
 * BaseActivity contains some modifications to the native AppCompatActivity.
 * Implements IBaseView
 * Mainly, it returns context from activity those extends BaseActivity
 * @param P is the type of the presenter for MVP
 */
abstract class BaseActivity<P : BasePresenter<IBaseView>> : IBaseView, AppCompatActivity() {

    lateinit var presenter: P

    /*
     *   Returns context
     */
    override fun getContext(): Context {
        return this
    }
}