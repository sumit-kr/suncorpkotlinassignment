package com.sun.corp.activities

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sun.corp.R
import corp.sun.com.suncorpkotlintest.model.Transaction
import kotlinx.android.synthetic.main.dialog_fragment_transaction.view.*

/*
 * DialogFragment used to display transaction detail
 */
class TransactionDialogFragment : DialogFragment() {

    lateinit var transaction: Transaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // fetch arguments of DialogFragment
        val bundle = arguments
        transaction = bundle?.getParcelable<Transaction>("transaction") as Transaction

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(transaction) {
            view.description.text = description
            view.amount.text = resources.getString(R.string.amount) + amount.toString()
            view.date.text = effectiveDate
        }
    }

    override fun onResume() {
        super.onResume()

        // Resize dialogfragment size respective to screen desnisty
        val widthPixels = resources.displayMetrics.widthPixels * 0.90
        val heightPixels = resources.displayMetrics.heightPixels * 0.70

        dialog.window.setLayout(widthPixels.toInt(), heightPixels.toInt())
    }

}